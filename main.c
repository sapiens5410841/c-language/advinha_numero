#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    int numeroAleatorio, tentativa, tentativasRestantes = 5;

    // Inicializando o gerador de numeros aleatorios com base no tempo atual
    srand(time(NULL));

    // Gerando um numero aleatorio entre 1 e 100
    numeroAleatorio = rand() % 100 + 1;

    printf("Bem-vindo ao jogo de adivinhacao!\n");
    printf("Tente adivinhar o numero secreto entre 1 e 100.\n");

    // Loop do jogo
    do {
        printf("Voce tem %d tentativas restantes.\n", tentativasRestantes);
        printf("Digite seu palpite: ");
        scanf("%d", &tentativa);

        // Verificando o palpite do jogador
        if (tentativa > numeroAleatorio) {
            printf("Tente um numero menor.\n");
        } else if (tentativa < numeroAleatorio) {
            printf("Tente um numero maior.\n");
        } else {
            printf("Parabens! Voce adivinhou o numero secreto %d!\n", numeroAleatorio);
            break; // Sai do loop do jogo
        }

        tentativasRestantes--;
    } while (tentativasRestantes > 0);

    // Se o jogador esgotar todas as tentativas sem adivinhar o numero
    if (tentativasRestantes == 0) {
        printf("Suas tentativas acabaram. O numero secreto era %d.\n", numeroAleatorio);
    }

    return 0;
}
